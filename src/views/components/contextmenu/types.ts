import { Folder } from '@/types/global';

export type ContextmenuData = {
  title: string;
  action: (...arg: any) => any;
  icon?: string;
  disabled?: boolean;
};
export type ContextmenuConfig = {
  left: number;
  top: number;
  show: boolean;
  list: ContextmenuData[];
  /* 目标节点,右键菜单挂到这个位置 */
  target?: Folder | null;
};
