import { ref } from 'vue';
import { ContextmenuConfig } from './types';

// eslint-disable-next-line import/prefer-default-export
export const contextmenuConfig = ref<ContextmenuConfig>({
  left: 0,
  top: 0,
  show: false,
  list: [],
  target: null,
});
