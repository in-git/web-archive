import { Folder } from '@/types/global';
import { formatDate } from '@/utils/utils';
import { ref } from 'vue';

export const show = ref(false);
export const autoComplete = ref({
  autoGetTitle: true,
  autoGetIcon: true,
});
const folderObejct: Folder = {
  link: '',
  id: '',
  title: '',
  editMode: false,
  type: 'file',
  date: formatDate(new Date()),
  sort: -1,
};
export const newConfig = ref<Folder>({
  ...folderObejct,
});
export const resetConfig = () => {
  newConfig.value = { ...folderObejct };
  autoComplete.value = {
    autoGetTitle: true,
    autoGetIcon: true,
  };
};

export const createAndEdit = () => {
  show.value = true;
};
export function removeQueryString(url: string): string {
  const index = url.indexOf('?');
  if (index !== -1) {
    return url.substring(0, index);
  }
  return url;
}
