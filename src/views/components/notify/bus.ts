import { ref } from 'vue';

type Notify = {
  show?: boolean;
  msg: string;
  title: string;
  onCancle?: () => any;
  onComfirm?: () => any;
};

const notifyObject: Notify = {
  show: false,
  msg: '',
  title: '',
};
export const notifyConfig = ref<Notify>({
  ...notifyObject,
});

export const notify = (config: Notify) => {
  notifyConfig.value = config;
  notifyConfig.value.show = true;
};
export const resetConfig = () => {
  notifyConfig.value = {
    ...notifyObject,
  };
};
