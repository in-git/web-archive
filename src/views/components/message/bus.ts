import { ref } from 'vue';

type MessageType = {
  show: boolean;
  msg: string;
};

export const messageConfig = ref<MessageType>({
  show: false,
  msg: '',
});
export const message = (msg: string) => {
  messageConfig.value.msg = msg;
  messageConfig.value.show = true;
  const flag = setTimeout(() => {
    clearTimeout(flag);
    messageConfig.value.show = false;
  }, 3000);
};
