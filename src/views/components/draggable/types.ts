export interface Position {
  x: number;
  y: number;
}
export interface DomRect {
  width: number;
  height: number;
}
