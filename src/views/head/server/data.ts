import { ServerOptions } from '@/store/program/types';

// eslint-disable-next-line import/prefer-default-export
export const defaultServeList: ServerOptions[] = [
  {
    title: '本地',
    baseUrl: 'http://localhost:8001/',
    static: true,
    id: '1',
  },
  {
    title: '局域网',
    baseUrl: 'http://192.168.0.166:8001/',
    static: true,
    id: '2',
  },
  {
    title: '主站[第一次使用请选这个]',
    baseUrl: 'http://150.158.14.110:8001/',
    static: true,
    id: '3',
  },
];
