import { ShareData } from '@/store/program/types';
import { programData } from '@/store/program/utils';
import { message } from '@/views/components/message/bus';
import axios from 'axios';
import { ref } from 'vue';

export const list = ref<ShareData[]>([]);
export const getData = async () => {
  const data = programData();
  try {
    list.value = [];
    if (!data.net.baseUrl) return;
    const { data: result } = await axios.get(data.net.baseUrl);
    if (result.data) {
      list.value = result.data;
    }
    list.value = result;
  } catch (error) {
    message('服务器未连接,请修改服务器地址');
  }
};
