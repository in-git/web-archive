import { Component, markRaw, ref } from 'vue';
import TreeVue from '../tree/Tree.vue';
import CatalogueVue from '../catalogue/Catalogue.vue';

export type Tab = {
  title: string;
  flag: 'list' | 'tree';
  component: Component | null;
};

export const tabs = ref<Tab[]>([
  {
    title: '根列表',
    flag: 'list',
    component: markRaw(CatalogueVue),
  },
  {
    title: '网页目录',
    component: markRaw(TreeVue),
    flag: 'tree',
  },
]);

export const currentTab = ref<Tab>(tabs.value[0]);
export const selectTabByFlag = (flag: 'list' | 'tree') => {
  tabs.value.forEach((e) => {
    if (e.flag === flag) {
      currentTab.value = e;
    }
  });
};
