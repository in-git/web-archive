export const commonTlds = [
  'com',
  'cn',
  'net',
  'org',
  'gov',
  'edu',
  'mil',
  'int',
  'info',
  'biz',
  'mobi',
  'xxx',
  'xyz',
  'top',
  'club',
  'vip',
  'pro',
  'shop',
  'site',
  'online',
];

export const searchEngines = [
  {
    title: '百度',
    query: 'https://www.baidu.com/s?wd=',
    site: 'https://www.baidu.com',
  },
  {
    title: 'Google',
    query: 'https://www.google.com/search?q=',
    site: 'https://www.google.com',
  },
  {
    title: '必应',
    query: 'https://cn.bing.com/search?q=',
    site: 'https://cn.bing.com',
  },
  {
    title: '搜狗',
    query: 'https://www.sogou.com/web?query=',
    site: 'https://www.sogou.com',
  },
  {
    title: '360',
    query: 'https://www.so.com/s?q=',
    site: 'https://www.so.com',
  },
  {
    title: '神马',
    query: 'https://m.sm.cn/s?q=',
    site: 'https://m.sm.cn',
  },
  {
    title: '知乎',
    query: 'https://www.zhihu.com/search?q=',
    site: 'https://www.zhihu.com',
  },
  {
    title: 'B站',
    query: 'https://search.bilibili.com/all?keyword=',
    site: 'https://www.bilibili.com',
  },
  {
    title: '豆瓣',
    query: 'https://www.douban.com/search?q=',
    site: 'https://www.douban.com',
  },
];
