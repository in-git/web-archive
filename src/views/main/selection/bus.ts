import { ref } from 'vue';

export type Selection = {
  left: number;
  top: number;
  show: boolean;
  width: number;
  height: number;
};
export const selectionConfig = ref<Selection>({
  left: 0,
  top: 0,
  show: false,
  width: 0,
  height: 0,
});
