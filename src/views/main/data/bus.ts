import deleteIcon from '@/assets/images/delete.png';
import edit from '@/assets/images/edit.png';
import openPng from '@/assets/images/open.png';
import folder from '@/assets/images/folder.png';
import gear from '@/assets/images/gear.png';
import google from '@/assets/images/google.png';
/* 图片 */
import { findById } from '@/store/program/utils';
import { Folder } from '@/types/global';
import { formatDate, openLink } from '@/utils/utils';
import { activeId, currentFolder } from '@/views/bus';
import { contextmenuConfig } from '@/views/components/contextmenu/bus';
import { notify } from '@/views/components/notify/bus';
import { ref } from 'vue';
import {
  newConfig,
  createAndEdit,
  resetConfig,
} from '@/views/components/create/bus';
import plus from '@/assets/images/plus.png';
import { nanoid } from 'nanoid';
import { openBrowser } from '@/views/browser/bus';
import { message } from '@/views/components/message/bus';

export const selectedKey = ref<Set<string>>(new Set());
export const open = (id?: string) => {
  const targetId = id || Array.from(selectedKey.value)[0];
  const target = findById(targetId);
  if (target?.type === 'file') {
    openLink(target.link || '');
  } else if (target) {
    currentFolder.value = target;
  }
};

export const update = (item: Folder) => {
  item.editMode = false;
  if (item.title.length === 0) {
    return;
  }
  if (currentFolder.value.children) {
    currentFolder.value.children = currentFolder.value.children.map((v) => {
      if (v.id === item.id) {
        v = item;
      }
      return v;
    });
  }
};
export const onContextmenu = (item: Folder) => {
  const e = window.event as MouseEvent;
  e.preventDefault();
  const target = findById(item.id);
  if (!target) return;

  contextmenuConfig.value = {
    show: true,
    left: e.x,
    top: e.y,
    target,
    list: [
      {
        icon: openPng,
        action() {
          if (item.type === 'file') {
            openLink(item.link || '');
          } else {
            currentFolder.value = target;
          }
        },
        title: '打开',
      },
      {
        icon: google,
        action() {
          if (item.type === 'file') {
            if (!item.link) {
              message('链接出了点问题,请在新窗口打开');
              return;
            }
            openBrowser({
              src: item.link,
              title: item.title,
              id: nanoid(),
              update: 0,
            });
          }
        },
        title: '浏览器',
      },
      {
        icon: gear,
        action() {
          newConfig.value = target;
          createAndEdit();
        },
        title: '编辑',
        disabled: item.type !== 'file',
      },
      {
        icon: edit,
        action() {
          item.editMode = true;
        },
        title: '重命名',
      },

      {
        icon: deleteIcon,
        action() {
          notify({
            msg: '将会删除这个文件',
            title: '警告',
            onComfirm() {
              currentFolder.value.children =
                currentFolder.value.children?.filter((v) => {
                  return v.id !== target.id;
                });
            },
          });
        },
        title: '删除',
      },
    ],
  };
};

export const createSubFolder = () => {
  if (currentFolder.value.children) {
    const id = nanoid();
    activeId.value = id;
    currentFolder.value.children.push({
      title: '未命名',
      editMode: true,
      id,
      type: 'folder',
      date: formatDate(new Date()),
      children: [],
      sort: currentFolder.value.children.length + 10000,
    });
  }
};
export const containerMenu = () => {
  const e = window.event as MouseEvent;
  e.preventDefault();
  contextmenuConfig.value = {
    show: true,
    left: e.x,
    top: e.y,
    list: [
      {
        title: '新建文件',
        icon: plus,
        action() {
          createAndEdit();
          resetConfig();
        },
      },
      {
        title: '新建文件夹',
        icon: folder,
        action() {
          createSubFolder();
        },
      },
    ],
  };
};
