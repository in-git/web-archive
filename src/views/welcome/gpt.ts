export default [
  {
    title: 'gpt',
    desc: '常用的GPT链接',
    category: 'ai',
    data: [
      {
        title: 'gpt',
        editMode: false,
        id: 'zGhl4a8Pmfk5QCOPA6t4E',
        type: 'folder',
        date: '2023-10-16 12:37:41',
        children: [
          {
            link: 'https://chat12.aichatos.xyz/#/chat/1689754095394',
            id: 'HJYhoxYsODHrBT9iE12WT',
            title: '常用gpt',
            editMode: false,
            type: 'file',
            date: '2023-10-16 12:37:39',
            sort: 11,
          },
          {
            link: 'https://wijy8o.aitianhu.online/#/chat/1002',
            id: 'eBC2RmeUgTqKBEh4Sd9FB',
            title: 'wijy8o.aitianhu',
            editMode: false,
            type: 'file',
            date: '2023-10-16 13:36:10',
            sort: 2,
          },
          {
            link: 'https://yiyan.baidu.com/welcome',
            id: 'b3GPtlV_Ll-z_e2z_XDXJ',
            title: '文心一言',
            editMode: false,
            type: 'file',
            date: '2023-10-16 13:36:10',
            sort: 3,
          },
          {
            link: 'https://ro1vfa.aitianhu.online/#/chat/1002',
            id: '4xRnBzvj5j7jMxYS7XiPV',
            title: 'ro1vfa.aitianhu',
            editMode: false,
            type: 'file',
            date: '2023-10-16 13:36:10',
            sort: 4,
          },
          {
            link: 'https://chataa.free2gpt.xyz/',
            id: 'YwDdY9VFU5hF_e1O_4ndx',
            title: 'free2gpt',
            editMode: false,
            type: 'file',
            date: '2023-10-16 13:52:37',
            sort: 8,
          },
          {
            link: 'https://chat.tinycms.xyz:3002/',
            id: 'SAN9QqJEgVcymJ6EkTKvG',
            title:
              'ChatGPTPlus-智能AI聊天工具-免费ChatGPT网站在线体验无限制使用',
            editMode: false,
            type: 'file',
            date: '2023-10-16 13:52:37',
            sort: 7,
          },
          {
            link: 'https://toyaml.com/chat.html',
            id: 'XMyPWVOfrnLxbghOUe05L',
            title: 'ChatGPT在线聊天机器人-ToYaml.com',
            editMode: false,
            type: 'file',
            date: '2023-10-16 13:55:46',
            sort: 8,
          },
          {
            link: 'https://c.binjie.fun/',
            id: '3vlg5MYhnlJQf1hxI9TOO',
            title: 'AIchatOS',
            editMode: false,
            type: 'file',
            date: '2023-10-16 13:55:46',
            sort: 8,
          },
          {
            link: 'https://www.ai-eye.org/',
            id: 'IXMPMrugZOeoKBRrxewwY',
            title: 'ai-eye',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:16:30',
            sort: 9,
          },
          {
            link: 'https://xinghuo.xfyun.cn/',
            id: 'uNObWUbGiDGj8WTaMeBCf',
            title: '讯飞星火认知大模型-AI大语言模型-星火大模型-科大讯飞',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:16:30',
            sort: 0,
          },
          {
            link: 'https://tongyi.aliyun.com/',
            id: 'i8wJS3rBagCzzk0CnXFuz',
            title: '通义大模型',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:16:30',
            sort: 5,
          },
          {
            link: 'http://in-git.gitee.io/in-nav/#/',
            id: 'cZLl-YLG2Nb6pJLFpA5du',
            title: '轻音集群加载中...',
            editMode: false,
            type: 'file',
            date: '2023-10-16 15:42:42',
            sort: 10,
          },
          {
            link: 'https://faucet.openkey.cloud/',
            id: 'CqF63K9gbn94JsZvl7eXW',
            title: '水龙头',
            editMode: false,
            type: 'file',
            date: '2023-10-24 12:7:1',
            sort: 12,
          },
          {
            link: 'https://chat.nextweb.fun',
            id: '4w_SiIjq7VLQ6VBGnMhzF',
            title: 'gpt',
            editMode: false,
            type: 'file',
            date: '2023-10-24 12:7:1',
            sort: 13,
          },
        ],
        sort: 0,
        showChildren: false,
      },
      {
        title: '图像处理',
        editMode: false,
        id: 'xQgBhazg2xygiZqVhyqHW',
        type: 'folder',
        date: '2023-10-16 14:33:42',
        children: [
          {
            link: 'https://aigc.wondershare.cn/',
            id: '3Qfw9aoVYJVcksHYeyWYe',
            title: '万兴爱画-AI生成艺术创意灵感平台',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:16:30',
            sort: 0,
          },
          {
            link: 'http://portal.yjai.art/',
            id: 'fnBlDOEKn6mNp4SS0Aa_7',
            title: '意间AI',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:16:30',
            sort: 1,
          },
          {
            link: 'https://luban.aliyun.com/',
            id: 'DBXFDG_ffWTKBk5EHwDvJ',
            title: '鹿班- 让设计更美好',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:16:30',
            sort: 3,
          },
          {
            link: 'https://halfwork.cn/',
            id: '1MCeuKqc_LF-Vx3v519Et',
            title: 'halfwork',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:16:30',
            sort: 4,
          },
          {
            link: 'https://www.logosc.cn/',
            id: 'treod53PTcO9sTOon1IUK',
            title: 'LOGO设计神器；公司logo在线设计生成器- 标小智LOGO神器',
            editMode: false,
            type: 'file',
            date: '2023-10-16 15:52:53',
            sort: 4,
          },
        ],
        sort: 11,
        showChildren: false,
      },
      {
        title: 'AI文本',
        editMode: false,
        id: 'NzgQGKMohBkfYjMY-Z9av',
        type: 'folder',
        date: '2023-10-16 14:40:17',
        children: [
          {
            link: 'https://xiezuocat.com/',
            id: 'sgQl0QnRiuhb8xF3GdFI6',
            title: '秘塔写作猫',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:36:45',
            sort: 0,
          },
          {
            link: 'https://ibiling.cn/home',
            id: '8lUAeZkZBZVCR2Exqv2UA',
            title: '笔灵AI写作-ai智能写作-在线AI写作生成器',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:36:45',
            sort: 1,
          },
          {
            link: 'https://shutu.cn/',
            id: 'GLMSfiOP94r0OGoaBoDy2',
            title: 'TreeMind树图-免费在线AI思维导图专业制作工具与模板软件',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:36:45',
            sort: 2,
          },
          {
            link: 'https://www.heyfriday.cn/home',
            id: 'p5s3k1T-qnwuLglR0vCmm',
            title: 'HeyFriday- 智能AI写作工具（星期五）',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:36:45',
            sort: 3,
          },
          {
            link: 'https://web.mypitaya.com/login',
            id: 'VKNKJfyA46Q451Cqcy3xS',
            title: '火龙果',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:36:45',
            sort: 4,
          },
          {
            link: 'https://www.writingo.net/document',
            id: '23igRx0NvhBL12P7pM0u4',
            title: 'writingo',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:41:38',
            sort: 5,
          },
          {
            link: 'https://www.giiso.com/#/',
            id: 'HuIj00fKArpBGG9qaigpr',
            title: 'Giiso写作机器人，一款内容创作AI辅助工具',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:41:38',
            sort: 6,
          },
          {
            link: 'https://www.langboat.com/portal/ai-content',
            id: 'b5JpOUgXivYR_J15edaB7',
            title: 'AIGC(智能创作)平台| 澜舟科技-业界领先的认知智能公司编组',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:41:38',
            sort: 7,
          },
          {
            link: 'https://www.aigaixie.com/',
            id: 'PFVnTYl9m-9pB1lBN3iX2',
            title: '爱改写- AI在线人工智能文字生产力工具',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:41:38',
            sort: 8,
          },
          {
            link: 'https://chatdoc.com/',
            id: 'mhdDWbMJ41Q3O5ffYLgYh',
            title: 'chatdoc',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:41:38',
            sort: 9,
          },
          {
            link: 'http://work.xiaobote.com/#/index/home',
            id: '0MyJJcyLVEE7cP6nJXGhD',
            title: '销博特',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:41:38',
            sort: 10,
          },
        ],
        sort: 0,
        showChildren: false,
      },
      {
        title: 'AI办公',
        editMode: false,
        id: '3aDuOMiXpTwPC2P8TVun7',
        type: 'folder',
        date: '2023-10-16 14:45:17',
        children: [
          {
            link: 'https://www.x-design.com/',
            id: 'p4SOea21iLeMUrvjhnSa2',
            title:
              '美图设计室-智能生成海报-一键生成-免费设计平面设计编辑软件-免费模板图文素材',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:41:38',
            sort: 0,
          },
          {
            link: 'https://chatexcel.com/',
            id: '1z6kxTbe0XiE0LbwvhsQS',
            title: '酷表ChatExcel',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:41:38',
            sort: 1,
          },
          {
            link: 'https://www.feishu.cn/product/minutes',
            id: 's8VlvUdKgRsEN_FynWRGz',
            title:
              '飞书妙记-智能会议纪要,快捷语音识别转文字,将会议交流沉淀为知识,一切皆可妙记!',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:41:38',
            sort: 2,
          },
          {
            link: 'https://tinywow.com/tools/pdf',
            id: 'dTmCUlvLHTkqnSiNv7aBy',
            title:
              '\n                   Free Online PDF Tools - TinyWow\n            ',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:41:38',
            sort: 3,
          },
          {
            link: 'https://js.design/home',
            id: 'OnYSSI7WkQ9U4Y2qU4nfI',
            title: '即时设计- 可实时协作的专业 UI 设计工具',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:41:38',
            sort: 4,
          },
          {
            link: 'https://yinian.cloud.baidu.com/creativity/index',
            id: 'GyiWWMRqFQrpJRzVywP3Z',
            title: '百度智能云一念',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:41:38',
            sort: 5,
          },
          {
            link: 'https://www.chatpdf.com/',
            id: 'kvH7rxLVCC0HHdkPssrGq',
            title: 'chatpdf',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:41:38',
            sort: 6,
          },
          {
            link: 'https://boardmix.cn/',
            id: 'Yxx480jHoURYDag__ZvGs',
            title: 'boardmix博思白板，多人实时协作的流程图，思维导图工具',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:41:38',
            sort: 7,
          },
          {
            link: 'https://www.notion.so/product',
            id: '5Ko9seryLo3yJxWcEanTe',
            title: 'Yourconnected workspace for wiki, docs & projects | Notion',
            editMode: false,
            type: 'file',
            date: '2023-10-16 14:41:38',
            sort: 8,
          },
        ],
        sort: 11,
        showChildren: false,
      },
    ],
    id: 'oWv1JOHVJDwKZanbZUsUv',
    date: '2023-10-16 12:25:50',
  },
];
