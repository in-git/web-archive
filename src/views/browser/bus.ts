import { Component, computed, markRaw, reactive, ref } from 'vue';
import { nanoid } from 'nanoid';
import { message } from '../components/message/bus';

export type BrowserItem = {
  src: string;
  title: string;
  id: string;
  update: 0;
};

export const showBrowser = ref(false);

export const links = ref<BrowserItem[]>([]);

export const browserList = computed((): BrowserItem[] => {
  return links.value;
});

export const currentLink = ref<BrowserItem>({
  src: 'https://vue3.youlai.tech/#/system/user',
  title: 'VUE3',
  id: nanoid(),
  update: 0,
});
export const openBrowser = (config: BrowserItem) => {
  currentLink.value = config;
  showBrowser.value = true;
  if (links.value.length === 10) {
    message('最多打开10个标签');
    return;
  }
  links.value.unshift({
    ...config,
  });
};
