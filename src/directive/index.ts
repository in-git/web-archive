import { App } from 'vue';
import draggable from './draggable';
import focus from './focus';
import selected from './selected';
import dragEffect from './dragEffect';

export default {
  install(Vue: App) {
    Vue.directive('draggable', draggable);
    Vue.directive('focus', focus);
    Vue.directive('dragEffect', dragEffect);
    Vue.directive('selected', selected);
  },
};
