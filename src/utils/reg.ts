export const isURL = (str: string) => {
  // 使用正则表达式匹配网址的模式
  const pattern =
    /^(http|https):\/\/([a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})(:\d+)?([\\/\w.-]*)*$/;

  // 使用正则的test方法进行匹配判断
  if (pattern.test(str.split('#')[0])) {
    return true;
  }
  return false;
};
export const extractDomainKeyword = (url: string): string => {
  try {
    const match = url.match(
      /^(https?:\/\/)?(www\.)?([a-z0-9-]+)\.[a-z]+(\/|$)/i
    );
    if (match) {
      return match[3];
    }
  } catch (error) {
    // console.error(error);
  }
  return '';
};

/* 匹配IP类型的网址 */
export const matchIPUrl = (url: string): boolean => {
  const regex =
    /^https?:\/\/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{1,5}(\/\w+)*$/;
  return regex.test(url);
};
