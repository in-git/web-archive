import { createApp } from 'vue';
import { createPinia } from 'pinia';
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate';
import directive from './directive';
import App from './App.vue';
import 'in-less';
import '@/assets/style/index.scss';

const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);
const app = createApp(App);

app.use(directive);
app.use(pinia);

app.mount('#app');
