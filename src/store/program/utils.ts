import { Folder } from '@/types/global';
// eslint-disable-next-line import/no-cycle
import { selectList } from '@/views/bus';
import folderStore from '.';

export const programData = () => {
  return folderStore().$state;
};

export const findById = (id: string): Folder | undefined => {
  let target: Folder | undefined;
  const find = (items: Folder[]) => {
    // eslint-disable-next-line no-restricted-syntax
    for (const item of items) {
      if (item.id === id) {
        // 找到匹配的元素
        target = item;
        break;
      } else if (item.type === 'folder' && item.children) {
        find(item.children);
      }
    }
  };

  if (selectList.value) find(selectList.value);

  return target;
};
export const deleteById = (targetId: string) => {
  const del = (id: string, data: Folder[]) => {
    for (let i = 0; i < data.length; i += 1) {
      if (data[i].id === id) {
        data.splice(i, 1);
        return true;
      }
      if (data[i].children) {
        if (del(id, data[i].children || [])) {
          return true;
        }
      }
    }
    return false;
  };
  return del(targetId, selectList.value || []);
};
