import { defineStore } from 'pinia';
import { Program } from './types';

const folderStore = defineStore('folder', {
  state: (): Program => ({
    programList: [],
    defaultId: '',
    net: {
      baseUrl: '',
      title: '',
    },
    serverList: [],
  }),
  persist: true,
});

export default folderStore;
