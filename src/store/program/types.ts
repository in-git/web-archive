import { Folder } from '@/types/global';

export type ServerOptions = {
  title: string;
  baseUrl: string;
  edit?: boolean;
  id: string;
  static?: boolean;
};

export interface ProgramData {
  title: string;
  desc: string;
  category: string;
  data: Folder[];
  id: string;
  date: string;
}
export interface Net {
  baseUrl: string;
  title: string;
}
export interface Program {
  programList: ProgramData[];
  /* 默认打开的ID */
  defaultId: string;
  net: Net;
  serverList: ServerOptions[];
}

export interface ShareData extends ProgramData {
  username: string;
}
