import { defineStore } from 'pinia';
import { ISystem } from './type';

const systemStore = defineStore('system', {
  state: (): ISystem => ({
    welcome: true,
  }),
  persist: true,
});

export default systemStore;
