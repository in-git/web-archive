import systemStore from './system';

// eslint-disable-next-line import/prefer-default-export
export const getSystem = () => {
  return systemStore().$state;
};
