export interface Folder {
  title: string;
  editMode: boolean;
  id: string;
  type: 'folder' | 'file' | '';
  link?: string;
  children?: Folder[];
  showChildren?: boolean;
  date: string;
  sort: number;
}
